#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
  Copyright (c) 2020, Nagoya University
  All rights reserved.

  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions are met:

  * Redistributions of source code must retain the above copyright notice, this
    list of conditions and the following disclaimer.

  * Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.

  * Neither the name of Autoware nor the names of its
    contributors may be used to endorse or promote products derived from
    this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
"""
import sys
import rosbag
import argparse

__author__ = "Alexander Carballo"
__email__ = "alexander@g.sp.m.is.nagoya-u.ac.jp"

class FixBagTimestamp:
    def __init__(self, input, output):
        print("Opening input BAG")
        inbag = rosbag.Bag(input)
        messages = inbag.get_message_count()
        print("Input BAG has {} messages".format(messages))
        try:
            import progressbar
            bar = progressbar.ProgressBar(widgets=[progressbar.Bar(), progressbar.SimpleProgress(format='(%(value_s)s of %(max_value_s)s)'), 
                progressbar.Timer(), progressbar.AdaptiveETA()], max_value = messages, redirect_stdout=True)
            bar.start()
        except:
            print('Working, please wait...')
            bar = None

        i = 0
        with rosbag.Bag(output, 'w') as outbag:
            for topic, msg, t in inbag.read_messages():
                # This replaces the input topic timestamp by the bag timestamp
                msg.header.stamp = t
                outbag.write(topic, msg, t)
                if bar:
                    bar.update(i)
                i = i + 1
        if bar:
            bar.finish()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description = "Change timestamps in BAG file")
    parser.add_argument("-i", "--input", required=True, help = "Input BAG filename.")
    parser.add_argument("-o", "--output", default = "output.bag", help = "Output BAG filename.")
    args = parser.parse_args()

    if args.input == None:
        print("Input BAG filename not given.")
        parser.print_help()
        sys.exit(1)

    # process bag file
    bagtimestamp = FixBagTimestamp(args.input, args.output)
